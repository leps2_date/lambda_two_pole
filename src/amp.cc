#include "spinor_mat.h"
#include "amp.h"


TComplex Amp_c(double Eg, double costh, int hel_gamma, int mi, int mf) {
  TLorentzVector k1(0, 0, Eg, Eg), p1(0, 0, 0, Mp);  // lab system
  TLorentzVector W = k1 + p1;

  // std::cout << "Lab" << std::endl;
  // W.Print();
  // std::cout << "s : " << W.Mag() << std::endl;
  // k1.Print();
  // p1.Print();

  /* lab --> CM */
  k1.Boost(-W.BoostVector());
  p1.Boost(-W.BoostVector());
  W = k1 + p1;  

  // std::cout << "CM" << std::endl; 
  // W.Print();
  // std::cout << "s : " << W.Mag() << std::endl;
  // k1.Print();
  // p1.Print();
  /* momentum for K+ */
  TLorentzVector k2;
  double m3 = Mkaon;        // charged kaon mass
  double m4 = 1520 * MeV;
  double pdk = TMath::Sqrt((W.M2() - sq(m3 + m4)) * (W.M2() - sq(m4 - m3)))/(2 * W.M());
  k2.SetXYZM(pdk * TMath::Sqrt(1 - sq(costh)), 0, pdk * costh, m3);
  TLorentzVector p2 = k1 + p1 - k2;
  TLorentzVector qs = k1 + p1;

  TComplex eps_mu[4];
  FillPolVector(k1, hel_gamma, eps_mu);

  TComplex ep_mu[4],e0_mu[4],em_mu[4];
  double mom[3] = {p2.Px(),p2.Py(),p2.Pz()};
  TComplex em[3] = {1/sqrt(2),TComplex(0,1/sqrt(2)),0};
  TComplex e0[3] = {0,0,1};
  TComplex ep[3] = {-1/sqrt(2),TComplex(0,-1./sqrt(2.)),0};
 
  TComplex em_dot_p = 0;
  TComplex e0_dot_p = 0;
  TComplex ep_dot_p = 0;
  for(int i = 0;i<3;i++){
    em_dot_p += mom[i]*em[i];
    e0_dot_p += mom[i]*e0[i];
    ep_dot_p += mom[i]*ep[i];
  }

  ep_mu[0] = ep_dot_p/p2.M();
  ep_mu[1] = ep[0] + p2.Px() * ep_dot_p / (p2.M() * (p2.M() + p2.E()));
  ep_mu[2] = ep[1] + p2.Py() * ep_dot_p / (p2.M() * (p2.M() + p2.E()));
  ep_mu[3] = ep[2] + p2.Pz() * ep_dot_p / (p2.M() * (p2.M() + p2.E()));

  e0_mu[0] = e0_dot_p/p2.M();
  e0_mu[1] = e0[0] + p2.Px() * e0_dot_p / (p2.M() * (p2.M() + p2.E()));
  e0_mu[2] = e0[1] + p2.Py() * e0_dot_p / (p2.M() * (p2.M() + p2.E()));
  e0_mu[3] = e0[2] + p2.Pz() * e0_dot_p / (p2.M() * (p2.M() + p2.E()));

  em_mu[0] = em_dot_p/p2.M();
  em_mu[1] = em[0] + p2.Px() * em_dot_p / (p2.M() * (p2.M() + p2.E()));
  em_mu[2] = em[1] + p2.Py() * em_dot_p / (p2.M() * (p2.M() + p2.E()));
  em_mu[3] = em[2] + p2.Pz() * em_dot_p / (p2.M() * (p2.M() + p2.E()));

  /* amplitude calculation */
  TComplex amp = TComplex(0, 0);
  TComplex ep_dot_eps;
  TComplex e0_dot_eps;
  TComplex em_dot_eps;

  for (int i = 0; i < 4; i++) {
    ep_dot_eps += eps_mu[i]*TComplex::Conjugate(ep_mu[i]);
    e0_dot_eps += eps_mu[i]*TComplex::Conjugate(e0_mu[i]);
    em_dot_eps += eps_mu[i]*TComplex::Conjugate(em_mu[i]);
  }

  const double g_K_N_Lstar = 11.;
  double ConstValue = charge * g_K_N_Lstar  /  Mkaon ;
  
  if(mf == -3){
    amp =  ConstValue * barDot(u(p2, -1), Gamma5 * em_dot_eps * u(p1, mi));
    return amp;
  }
  if(mf == -1){
    amp =  ConstValue * (sqrt(1/3)*barDot(u(p2, 1), Gamma5 *e0_dot_eps * u(p1, mi)) +
        sqrt(2/3) * barDot(u(p2, -1), Gamma5 * ep_dot_eps * u(p1, mi)));
    return amp;
  }
  if(mf == 1){
    amp =  ConstValue * (sqrt(2/3)*barDot(u(p2, 1), Gamma5 * em_dot_eps * u(p1, mi)) +
        sqrt(1/3) * barDot(u(p2, -1), Gamma5 * e0_dot_eps * u(p1, mi)));
    return amp;
  }
  if(mf == 3){
    amp =  ConstValue * barDot(u(p2, 1), Gamma5 * ep_dot_eps * u(p1, mi));
    return amp;
  }
  return amp;
}


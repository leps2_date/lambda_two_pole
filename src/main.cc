#include <stdio.h>
#include <string.h>

#include <cstdio>
#include <cstring>

#include <TCanvas.h>
#include <TString.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH1.h>

#include "crossSection_SDM.h"
#include "spinor_mat.h"

#define GRAPH 1
#define LIN 0

// usable
#define USABLE1 1  // dsigma/dOmega
#define USABLE2 1  // dsigma/dE

const double Eth = (sq(Mkaon + 1520 *MeV ) - sq(Mp)) / (2 * Mp);
const double Eend = 3000;

TString figtitle = "uchSigstar/uchSigstar";

int main() {
  
  TLorentzVector p;
  double mass = 1000.;
  p.SetXYZM(40,40,200,mass);
  u(p,1).disp();
  TComplex ubaru = barDot(u(p,1),u(p,1));
    std::cout << "mass : " << mass << std::endl;
    std::cout << "ubaru : " << ubaru << std::endl;
  // -------------------------
  //   dSigma/dcosth
  //   // -------------------------
#if USABLE1
  const int nEne_1 = 5;
  double Eg_1[nEne_1] = {1454, 1660, 1880, 2110, 2350};

  const int nCos = 80;
  double min = -1, max = 1;
  double dcosth = (max - min) / nCos;
  double cos[nCos], dsigma_dcos[nEne_1][nCos];
  for (int j = 0; j < nEne_1; j++) {
    for (int i = 0; i < nCos; i++) {
      cos[i] = min + dcosth * (i + 0.5);
      dsigma_dcos[j][i] = dsigma_dcosth_hioki(Eg_1[j] * MeV, cos[i]);
    }
  }

  TCanvas *c0 = new TCanvas();
  TH1 *frame0 = c0->DrawFrame(-1, 0., 1, 5);
  frame0->SetTitle("; cos#theta ; d#sigma/dcos#theta [#mub]");
  //  TF1 *f0[nEne_1];
  TGraph *gr0[nEne_1];
  for (int i = 0; i < nEne_1; i++) {
    gr0[i] = new TGraph(nCos, cos, dsigma_dcos[i]);
    gr0[i]->SetMarkerStyle(20);
    gr0[i]->SetMarkerColor(2 + i);
    //    f0[i] = new TF1(Form("f0%d", i), Form("dsigma_dcosth(%lf, x)/(2*pi)",
    //    Eg_1[i]), -1., 1.); f0[i]->SetLineColor(2+i);
  }
  TLegend *leg0 = new TLegend(0.5, 0.6, 0.9, 0.9);
  leg0->SetFillStyle(0);
  leg0->SetBorderSize(0);
  for (int i = 0; i < nEne_1; i++) {
    leg0->AddEntry(gr0[i], Form("E_{#gamma} = %.1lf [GeV]", Eg_1[i]), "lp");
  }
  frame0->Draw();
  for (int i = 0; i < nEne_1; i++) {
    gr0[i]->Draw("P,same");
  }
  leg0->Draw("same");
  c0->Print("dsigma_dcos.pdf");
#endif

  // ---------------------
  //    dsigma/dE
  // ---------------------
#if USABLE2
  TCanvas *c1 = new TCanvas();
  TH1 *frame1 = c1->DrawFrame(1500, 0., Eend, 100);
  std::cout << " Eth : " << Eth << std::endl;
  frame1->SetTitle("; E_{#gamma} [MeV]; d#sigma [#mub]");
  frame1->Draw();
#if GRAPH
  const int nEne = 20;
  double Eg[nEne], sigma[nEne];
  for (int i = 0; i < nEne; i++) {
    Eg[i] = Eth + i * (Eend - Eth) / nEne;
    sigma[i] = dsigma(Eg[i] * MeV);
    printf("dsigma(Eg=%.2lf) = %lf \n ",  Eg[i], sigma[i]);
  }
  TGraph *gr1 = new TGraph(nEne, Eg, sigma);
  gr1->SetMarkerStyle(20);
  gr1->SetMarkerColor(kBlue + 3);
  gr1->Draw("LP, same");
#endif
#if LIN
  TF1 *f1 = new TF1("f1", "dsigma(x)", Eth, Eend);
  f1->SetLineColor(kBlue + 3);
  f1->Draw("L, same");
#endif
   c1->Print("c1.pdf");
#endif
  return 0;
}

#ifndef AMP_H_
#define AMP_H_

#include <TComplex.h>
#include "spinor_mat.h"

TComplex Amp_c(double Eg, double costh, int hel_gamma, int mi, int mf);
// 
#endif // AMP_H_
